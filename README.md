# Disclaimer
Scripts for configuring and building adhell3 apk without any installations.

# Available commands
- `adhell3 setup`<br/>
Download and configure all necessary things for compiling an apk (This needs only be done once).<br/>
Type `y` to accept the license agreement of Android SDK.

- `adhell3 clean setup`<br/>
Re-download everything again before configuring all necessary things for compiling an apk.

- `adhell3 build`<br/>
Only build the apk without getting the latest one from gitlab. Useful if you have local changes like coloring adhell3.

- `adhell3 clean build`<br/>
Build the apk by getting the latest source from gitlab (Knox libs and app.properties need to be copied in the same folder where the script is located).

# Usage
## Knox SDK
- Download latest Knox SDK and supportlib from https://seap.samsung.com/sdk/knox-android
- Extract `knoxsdk.jar` and `supportlib.jar` respectively from both zip files
- Rename the supportlib jar file name to `supportlib.jar` if it is not the case

## Adhell3 app.properties
- Create `app.properties` file with your favorite editor
- Put `package.name=your.package.name` in the first line of the file and replace `your.package.name` with your desired package name

## Windows x64
- Download the script `adhell3.cmd` locally to your machine in a folder
- Open a windows console (Press Windows symbol + R and type `cmd`)
- Go to the folder where you download the script
- Copy `knoxsdk.jar` and `supportlib.jar` to that folder
- Copy `app.properties` to that folder
- Type these in the windows console:
  - `adhell3 setup`
  - `adhell3 clean build`
- The apk is located at `adhell3-master\app\build\outputs\apk\debug\app-debug.apk`

## Linux x64
- Download the script `adhell3_linux.sh` locally to your machine in a folder
- Open a terminal console
- Go to the folder where you download the script
- Copy `knoxsdk.jar` and `supportlib.jar` to that folder
- Copy `app.properties` to that folder
- Type these in the terminal console:
  - `chmod +x adhell3_linux.sh`
  - `bash adhell3_linux.sh setup`
  - `bash adhell3_linux.sh clean build`
- The apk is located at `adhell3-master/app/build/outputs/apk/debug/app-debug.apk`

## Mac x64
- Download the script `adhell3_mac.sh` locally to your machine in a folder
- Open a terminal console
- Go to the folder where you download the script
- Copy `knoxsdk.jar` and `supportlib.jar` to that folder
- Copy `app.properties` to that folder
- Type these in the terminal console:
  - `chmod +x adhell3_mac.sh`
  - `bash adhell3_mac.sh setup`
  - `bash adhell3_mac.sh clean build`
- The apk is located at `adhell3-master/app/build/outputs/apk/debug/app-debug.apk`
 
# Credits
- wget: https://eternallybored.org/misc/wget/ (Windows)
- 7za: https://github.com/develar/7zip-bin/ (Windows)
- Android SDK: https://developer.android.com/studio/
- AdoptOpenJDK: https://github.com/AdoptOpenJDK/openjdk8-binaries